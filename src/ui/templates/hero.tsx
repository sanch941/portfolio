import React from 'react';
import { Brain } from './brain';
import Link from '@docusaurus/Link';

export const Hero = () => {
    return (
        <header className="header">
            <div className="header__welcome">
                <h1 className="header__welcome__text">
                    Make IT better with <br /> Sanzhar Shakhmerden
                </h1>

                <Link className="header__welcome__button" to="/docs">
                    Get started
                </Link>
            </div>

            <div className="header__brain">
                <Brain />
            </div>
        </header>
    );
};
