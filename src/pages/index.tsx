import React from 'react';
import Layout from '@theme/Layout';
import { Hero } from '../ui/templates/hero';

export default function Home(): JSX.Element {
    return (
        <Layout>
            <Hero />
        </Layout>
    );
}
