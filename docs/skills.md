---
title: "Skills"
---

- **Frontend:**
    - `React`, `react-native`
    - `Typescript`
    - `Graphql(Apollo)`
    - State managers: `redux`, `redux toolkit`, `effector`
    - Architectural methodologies: `feature sliced ​​design`, `atomic design`
    - Styling: `styled components`, `sass`, `postcss`
    - Bundlers: `webpack, rollup`    
    - Static site generators: `gatsby`
    - Documentation generators: `docusaurus`
    - Cms: `strapi cms`, `netlify cms`
    - Animations: `gsap`, `anime js`
    - Devtools and react devtools: `Profiler`, `Components`

- **Backend**:
    - `Node js`
    - Frameworks: `nest js`, `fastify`, `express`
    - Databases: `postgres`, `redis`
    - `Graphql(Apollo code first)`

- **Mobile**:
    - `react-native`
    - `reanimated`    
    - `Xcode`, `Android studio`
    - Publishing apps to Appstore, Google Play

- **Others**:
    - `Gitlab-ci`
    - `Sentry`
    - `Git`
    - `Docker`, `docker-compose`
    - `Ubuntu`
    - `Digital Ocean`        
    - `Vim`, `tmux`, `htop`, `pm2` i.e
    - Experience with payment systems
    - Experience with high load systems
    - Experience of team management



