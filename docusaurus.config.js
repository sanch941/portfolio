// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
    title: 'Sanzhar Shakhmerden | Portfolio',
    tagline: 'Dinosaurs are cool',
    url: 'https://sanchdev.com',
    baseUrl: '/',
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',
    favicon: 'img/favicon.ico',

    // GitHub pages deployment config.
    // If you aren't using GitHub pages, you don't need these.
    organizationName: 'facebook', // Usually your GitHub org/user name.
    projectName: 'docusaurus', // Usually your repo name.

    // Even if you don't use internalization, you can use this field to set useful
    // metadata like html lang. For example, if your site is Chinese, you may want
    // to replace "en" with "zh-Hans".
    i18n: {
        defaultLocale: 'en',
        locales: ['en']
    },

    presets: [
        [
            'classic',
            /** @type {import('@docusaurus/preset-classic').Options} */
            ({
                docs: {
                    sidebarPath: require.resolve('./sidebars.json')
                },
                blog: false,
                theme: {
                    customCss: require.resolve('./src/css/style.scss')
                },
                gtag: {
                    trackingID: 'G-RS4L5H32Q2'
                }
            })
        ]
    ],

    themeConfig:
        /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
        ({
            navbar: {
                logo: {
                    alt: 'My Site Logo',
                    src: 'img/logo.png'
                },
                items: [
                    {
                        type: 'search',
                        position: 'right'
                    },
                    {
                        type: 'doc',
                        docId: 'about',
                        position: 'left',
                        label: 'Docs'
                    },

                    {
                        href: 'https://github.com/sanch941',
                        label: 'GitHub',
                        position: 'right'
                    }
                ]
            },
            footer: {
                style: 'light',
                links: [
                    {
                        title: 'Links',
                        items: [
                            {
                                label: 'Linkedin',
                                to: 'https://www.linkedin.com/in/sanzhar-shakhmerden-b68436217/'
                            },
                            {
                                label: 'Github',
                                to: 'https://github.com/sanch941'
                            }
                        ]
                    }
                ]
                // copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`
            },
            prism: {
                theme: lightCodeTheme,
                darkTheme: darkCodeTheme
            }
        }),
    plugins: [
        [
            require.resolve('@cmfcmf/docusaurus-search-local'),
            {
                indexBlog: false
            }
        ],
        'docusaurus-plugin-sass'
    ]
};

module.exports = config;
